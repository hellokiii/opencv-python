import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
	_, frame = cap.read()

	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	upper_color = np.array([204,221,214])
	lower_color = np.array([158,100,40])

	mask = cv2.inRange(hsv, lower_color, upper_color)

	kernel = np.ones((5,5), np.uint8)
	erosion = cv2.erode(mask, kernel, iterations = 3)
	dilation = cv2.dilate(mask, kernel, iterations = 3)

	# filtered = cv2.bitwise_and(frame, frame, mask = mask)
	cv2.imshow('mask', mask)
	cv2.imshow('erosion', erosion)
	cv2.imshow('dilation', dilation)
	# cv2.imshow('filtered', filtered)	

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()