import cv2

img = cv2.imread('dark.jpg')
grayimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# ret, thres = cv2.threshold(grayimg, 50, 255, cv2.THRESH_BINARY)
mean = cv2.adaptiveThreshold(grayimg, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 115, 1)
gaus = cv2.adaptiveThreshold(grayimg, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115, 1)

cv2.imshow('img', img)
# cv2.imshow('gray', grayimg)
cv2.imshow('mean', mean)
cv2.imshow('gaus', gaus)

cv2.waitKey(0)
cv2.destroyAllWindows()