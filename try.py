import cv2
import numpy as np

def nothing(x):
	pass

cap = cv2.VideoCapture(0)
lg = cv2.VideoCapture(1)
cv2.namedWindow('mask')

while True:
	_, frame = cap.read()
	_, lt = lg.read()
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)


	lower_color = np.array([13,0,80])
	upper_color = np.array([116,92,175])

	mask = cv2.inRange(hsv, lower_color, upper_color)
	mask_inv = cv2.bitwise_not(mask)
	


	filtered = cv2.bitwise_and(frame, frame, mask = mask)
	filtered2 = cv2.bitwise_and(lt, lt, mask = mask_inv)

	res = filtered + filtered2
	
	cv2.imshow('res', res)
	# cv2.imshow('filtered', filtered)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()	