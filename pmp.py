import cv2
import numpy as np

img = cv2.imread('logo.jpg', cv2.IMREAD_COLOR)
img2 = cv2.imread('logo.jpg', cv2.IMREAD_GRAYSCALE)
img3 = cv2.imread('logo.jpg', cv2.IMREAD_UNCHANGED)
print(img.shape)
print(img2.shape)
print(img3.shape)

cv2.imshow('img', img)
cv2.imshow('img2', img2)
cv2.imshow('img3', img3)

cv2.waitKey(0)
cv2.destroyAllWindows()