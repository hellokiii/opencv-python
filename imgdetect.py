import cv2
import numpy as np

target = cv2.imread('target.jpg')
graytarget = cv2.cvtColor(target, cv2.COLOR_BGR2GRAY)

shot = cv2.imread('shot.jpg', )
w, h = shot.shape[::-1]

res = cv2.matchTemplate(graytarget, shot, cv2.TM_CCOEFF_NORMED)
threshold = 0.5
loc = np.where(res > threshold)

for pt in zip(*loc[::-1]):
	cv2.rectangle(target, pt, (pt[0]+w, pt[1]+w), (0,255,255), 2)

cv2.imshow('detected', target)


if cv2.waitKey(0) & 0xFF == ord('q'):
	cv2.destroyAllWindows()