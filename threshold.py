import cv2
import numpy as np

car = cv2.imread('mycar.jpg')
logo = cv2.imread('logo.jpg')

rows, cols, channels = logo.shape
roi = car[0:rows, 0:cols]

graylogo = cv2.cvtColor(logo, cv2.COLOR_BGR2GRAY)
ret, mask = cv2.threshold(graylogo, 20, 255, cv2.THRESH_BINARY)
mask_inv = cv2.bitwise_not(mask)
logo_fg = cv2.bitwise_and(logo, logo, mask=mask)
car_bg = cv2.bitwise_and(roi, roi, mask = mask_inv)

roi = logo_fg + car_bg
car[0:rows, 0:cols] = roi


# cv2.imshow('mask', logo_fg)
# cv2.imshow('car', car_bg)
cv2.imshow('res', car)


cv2.waitKey(0)
cv2.destroyAllWindows()