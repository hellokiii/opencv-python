#shows max and min hsv value to filter the color you want
import cv2
import numpy as np

def nothing(x):
	pass

cap = cv2.VideoCapture(0)
cv2.namedWindow('mask')

cv2.createTrackbar('max_h', 'mask', 255, 255, nothing)
cv2.createTrackbar('min_h', 'mask', 0, 255, nothing)
cv2.createTrackbar('max_s', 'mask', 255, 255, nothing)
cv2.createTrackbar('min_s', 'mask', 0, 255, nothing)
cv2.createTrackbar('max_v', 'mask', 255, 255, nothing)
cv2.createTrackbar('min_v', 'mask', 0, 255, nothing)

while True:
	_, frame = cap.read()

	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	max_h = cv2.getTrackbarPos('max_h', 'mask')
	min_h = cv2.getTrackbarPos('min_h', 'mask')
	max_s = cv2.getTrackbarPos('max_s', 'mask')
	min_s = cv2.getTrackbarPos('min_s', 'mask')
	max_v = cv2.getTrackbarPos('max_v', 'mask')
	min_v = cv2.getTrackbarPos('min_v', 'mask')



	lower_color = np.array([min_h,min_s,min_v])
	upper_color = np.array([max_h,max_s,max_v])

	mask = cv2.inRange(hsv, lower_color, upper_color)
	filtered = cv2.bitwise_and(frame, frame, mask = mask)
	cv2.imshow('mask', mask)
	cv2.imshow('filtered', filtered)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		print('max_h: {0}'.format(max_h))
		print('min_h: {0}'.format(min_h))
		print('max_s: {0}'.format(max_s))
		print('min_s: {0}'.format(min_s))
		print('max_v: {0}'.format(max_v))
		print('min_v: {0}'.format(min_v))
		break

cap.release()
cv2.destroyAllWindows()