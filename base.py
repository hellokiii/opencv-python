import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:

	_, frame = cap.read()
	k = cv2.waitKey(1) & 0xFF
	if k == ord('q'):
		break
	
cap.release()
cv2.destroyAllWindows()