import cv2

cap = cv2.VideoCapture(0)
i = 1
while True:
	ret, frame = cap.read()
	cv2.imshow('frame', frame)

	k = cv2.waitKey(1) & 0xFF

	if k == ord('q'):
		break
	elif k == ord('s'):
		cv2.imwrite('capture/captured'+str(i)+'.jpg', frame)
		i+=1
		print('captured!')
	elif k == 255:
		i
	else:
		print(k)
		print('s: capture')
		print('q: quit')

cap.release()
cv2.destroyAllWindows()
