import cv2
import numpy as np
from time import sleep


# def mask(frame):
# 	grayframe = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
# 	_, mask = cv2.threshold(grayframe, 252, 255, cv2.THRESH_BINARY)
# 	# inv_mask = cv2.bitwise_not(mask)
# 	return(mask)

def merge(fgframe, bgframe, mask, inv_mask):
	masked_fg = cv2.bitwise_and(fgframe, fgframe, mask = mask)
	masked_bg = cv2.bitwise_and(bgframe, bgframe, mask = inv_mask)
	merged_frame = masked_fg + masked_bg

	return merged_frame



bg_video = cv2.VideoCapture(0)
fg_video = cv2.VideoCapture(1)
while True:
	ret1, bgframe = bg_video.read()
	ret2, fgframe = fg_video.read()


	if ret1 & ret2:




		grayframe = cv2.cvtColor(bgframe, cv2.COLOR_BGR2GRAY)
		_, mask = cv2.threshold(grayframe, 200, 255, cv2.THRESH_BINARY)
		inv_mask = cv2.bitwise_not(mask)

		# mask = mask(bgframe)
		# inv_mask = cv2.bitwise_not(mask)
		merged_frame = merge(fgframe, bgframe, mask, inv_mask)


		cv2.imshow('frames', bgframe)
		# cv2.imshow('threshFrame', threshFrame)
		cv2.imshow('merged', merged_frame)
	else:
		print('waiting for response')
		sleep(1)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()
