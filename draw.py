import cv2

img = cv2.imread('mycar.jpg', cv2.IMREAD_UNCHANGED)

cv2.line(img, (0,0), (300, 300), (255, 0, 0), 10)
cv2.rectangle(img, (200,200), (500, 500), (0, 255, 0), 10)
cv2.circle(img, (150,150), 100, (0, 0, 255), 5)

font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img, 'Bandiming', (50, 500), font, 1, (100,100,100))

cv2.imshow('image', img)

cv2.waitKey(0)
cv2.destroyAllWindows()